<?php

use DB\JSONDB;

spl_autoload_register(function ($nombre) {
	$nombre = str_replace('\\', '/', $nombre);
	require "Classes/$nombre.php";
});


$datos = [
	'nombre' => 'Bronco',
	'especie' => 'Perro',
	'humano_id' => '1',
];

$mascota = new Mascota;
$mascota->save($datos);
