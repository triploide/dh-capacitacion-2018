<?php

$host = 'localhost';
$db = 'vet';
$user = 'root';
$password = 'root';

try {
	$pdo = new PDO("mysql:host=$host;dbname=$db", $user, $password);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo->setAttribute(PDO::DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
} catch (Exception $e) {
	die($e->getMessage());
}

