<?php

function select($tabla, $id)
{
	global $pdo;

	$stmt = $pdo->prepare("SELECT * FROM $tabla WHERE id = :id");
	$stmt->bindValue(':id', $id);
	$stmt->execute();
	return $stmt->fetch();
}

function insert($tabla, $datos)
{
	global $pdo;

	$columnas = '';
	$placeholders = '';

	foreach ($datos as $columna => $valor) {
		$columnas .= "$columna,";
		$placeholders .= ":$columna,";
	}

	$columnas = trim($columnas, ',');
	$placeholders = trim($placeholders, ',');

	//$keys = array_keys($datos);
	//$columnas = implode(',', $keys);
	//$placeholders = ':'.implode(',:', $keys);

	$stmt = $pdo->prepare("INSERT INTO $tabla ($columnas) VALUES ($placeholders)");
	$stmt->execute($datos);
}

function update($tabla, $datos, $id)
{
	global $pdo;

	$set = '';

	foreach ($datos as $columna => $valor) {
		$set .= "$columna=:$columna,";
	}

	$set = trim($set, ',');

	$stmt = $pdo->prepare("UPDATE $tabla SET $set WHERE id = :id");
	$datos['id'] = $id;
	$stmt->execute($datos);
}

function delete($tabla, $id)
{
	global $pdo;

	$stmt = $pdo->prepare("DELETE FROM $tabla WHERE id = :id");
	//var_dump($stmt); exit;
	$stmt->bindValue(':id', $id);
	$stmt->execute();
}
