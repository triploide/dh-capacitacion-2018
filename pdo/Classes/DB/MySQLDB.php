<?php

namespace DB;

class MySQLDB extends DB
{
  private $pdo;

  public function __construct()
	{
		$host = 'localhost';
		$db = 'vet';
		$user = 'root';
		$password = 'root';

		try {
			$this->pdo = new PDO("mysql:host=$host;dbname=$db", $user, $password);
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

  public function insert($datos, $model)
  {
    $columnas = '';
		$placeholders = '';

		foreach ($datos as $columna => $valor) {
			$columnas .= "$columna,";
			$placeholders .= ":$columna,";
		}

		$columnas = trim($columnas, ',');
		$placeholders = trim($placeholders, ',');

		$stmt = $this->pdo->prepare("INSERT INTO $model->entidad ($columnas) VALUES ($placeholders)");
		$stmt->execute($datos);
  }

  public function update($datos, $model)
  {
  	$set = '';

  	foreach ($datos as $columna => $valor) {
  		$set .= "$columna=:$columna,";
  	}

  	$set = trim($set, ',');

  	$stmt = $this->pdo->prepare("UPDATE {$model->entidad} SET $set WHERE id = :id");
  	$datos['id'] = $model->id;
  	$stmt->execute($datos);
  }

  public function select()
  {

  }

  public function delete()
  {

  }
}
