<?php

namespace DB;

class JSONDB extends DB
{
  private $file;
  private $json;

  public function __construct()
	{
    $this->file = __DIR__ .'/../vet.json';
		$this->json = file_get_contents(__DIR__ .'/../vet.json');
    $this->json = json_decode($this->json, true);
	}

  public function insert($datos, $model)
  {
    $this->json[$model->entidad][] = $datos;
    file_put_contents( $this->file, json_encode($this->json) );
  }

  public function update($datos, $model)
  {
  	$set = '';

  	foreach ($datos as $columna => $valor) {
  		$set .= "$columna=:$columna,";
  	}

  	$set = trim($set, ',');

  	$stmt = $this->pdo->prepare("UPDATE {$model->entidad} SET $set WHERE id = :id");
  	$datos['id'] = $model->id;
  	$stmt->execute($datos);
  }

  public function select()
  {

  }

  public function delete()
  {

  }
}
