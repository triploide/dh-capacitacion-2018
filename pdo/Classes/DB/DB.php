<?php

namespace DB;

abstract class DB
{
  abstract public function insert($datos, $model);

  abstract public function update($datos, $model);

  abstract public function select();

  abstract public function delete();
}
