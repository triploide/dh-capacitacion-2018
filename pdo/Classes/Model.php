<?php

use DB\JSONDB;

 class Model
 {
   private $db; //Tipo DB
   public $entidad;
   public $id;

   public function __construct()
   {
     $this->db = new JSONDB; //DB
   }

   public function save($datos)
   {
     if ($this->id) {
       $this->update($datos);
     } else {
       $this->insert($datos);
     }
   }

   public function select()
   {
     return $this->db->select();
   }

   public function delete()
   {
     $this->db->delete();
   }

   public function insert($datos)
   {
     $this->db->insert($datos, $this);
   }

   public function update($datos)
   {
     $this->db->update($datos, $this);
   }

 }
